import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppComponent} from 'src/app/app.component'
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import{PoductComponent} from './poduct/poduct.component'



const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"signup", component:SignupComponent},
  {path:"poduct", component:PoductComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
