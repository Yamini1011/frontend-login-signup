import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import{HttpService} from 'src/app/service/http.service';
import {UserService} from 'src/app/service/user.service';
import {constant} from 'src/app/core/const'
import {Api} from 'src/app/core/api'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
login!:FormGroup;
submitted:boolean=false;
valid=false;
  alert: any;


  constructor(
private fb:FormBuilder,
private userser:UserService,
private httpxy:HttpClient,
private dataSharing:UserService,

  ) { }

  ngOnInit(): void {
    this.initForm();

  }
  initForm(){
    this.login = this.fb.group({
      email:['',[Validators.required,Validators.pattern(constant.EMAIL)]],
      password:['',[Validators.required,Validators.pattern(constant.PASSWORD)]]
    })
    console.log(this.login);
  }
  onSubmit(){
    console.log(this.login);
    this.submitted=true;
    if(!this.login.invalid){
      console.log(this.login.value);
      this.httpxy.post('http://localhost:2200/loginUser', this.login.value).subscribe((res:any) => {
        console.log(res);
        alert(res.message);
      },err => {
        console.log(err);
        alert(err.error.message); 
      });
      
    }  
  }
  }
