import { Injectable } from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs'
@Injectable({
  providedIn: 'root'
})
export class UserService {
  SharingData: BehaviorSubject<any> =  new BehaviorSubject(null);

  constructor(
    private router:Router
  ) { }
  afterLogin(data:any){
    localStorage.setItem("userLoginDetails", JSON.stringify(data));
    this.toHome();
  }
  toHome(){
    this.router.navigate(['/layout'])
  }
  logout(){
    localStorage.removeItem("userLoginDetails");
    this.router.navigate([""])
  }
    setData(value: any){
      console.log(value);
      setTimeout(() => {
        this.SharingData.next(value);
      },1000);
      localStorage.setItem('userDetails', JSON.stringify(value));
    }
  }
  