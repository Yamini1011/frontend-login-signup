import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from 'src/environments/environment'

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  post(arg0: string, value: any) {
    throw new Error('Method not implemented.');
  }

  constructor(
    private httpClient:HttpClient
  ) { }
  postData(api:any,data:any){
    return this.httpClient.post(environment.baseUrl+api, data)
    }
    }
    
