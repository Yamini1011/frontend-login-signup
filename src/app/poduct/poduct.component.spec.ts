import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PoductComponent } from './poduct.component';

describe('PoductComponent', () => {
  let component: PoductComponent;
  let fixture: ComponentFixture<PoductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PoductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PoductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
