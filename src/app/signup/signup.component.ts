import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import{FormGroup,FormBuilder,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { constant } from 'src/app/core/const';
import { UserService } from '../service/user.service';



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
  signup!: FormGroup;
  submitted:boolean=false;
  trueconfirmpassword=false;
  valid=false;
  alert: any;
  
  
  constructor(
    private fb:FormBuilder,
   private httpxy:HttpClient,
    private router:Router,
    private dataSharing:UserService) { }

  ngOnInit(): void {
    this.initForm();
  }
  initForm(){
    this.signup=this.fb.group({
    name:['',[Validators.required,Validators.minLength(3),Validators.maxLength(10),Validators.pattern(constant.NAME)]],
    email:['',[Validators.required,Validators.pattern(constant.EMAIL)]],
    password:['',[Validators.required,Validators.pattern(constant.PASSWORD)]],
    /* confirmPassword:['',[Validators.required]]},
      {validator: ConfirmedValidator('password', 'confirmPassword') */
    })
   /*  console.log(this.signup)
    function ConfirmedValidator(controlName: string, matchingControlName: string){
      return (formGroup: FormGroup) => {
          const control = formGroup.controls[controlName];
          const matchingControl = formGroup.controls[matchingControlName];
          if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
              return;
          }
          if (control.value !== matchingControl.value) {
              matchingControl.setErrors({ confirmedValidator: true });
          } else {
              matchingControl.setErrors(null);
          }
      }
    } */
  }
  onSubmit(){
    console.log(this.signup)
if (!this.signup.invalid){
  console.log(this.signup.value);
  this.httpxy.post('http://localhost:2200/addUser', this.signup.value).subscribe((res:any) =>{
    alert(res.message);
  }, err => {
    console.log(err);
    alert(err.error.message);
  }); 

console.log(this.signup);

      }
      
    } 
  }
